/*
 *      Copyright (C) 2014 Robert Stupp, Koeln, Germany, robert-stupp.de
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package histogramtest;

import java.util.Arrays;

import com.yammer.metrics.core.Histogram;
import com.yammer.metrics.core.MetricsRegistry;
import com.yammer.metrics.stats.Snapshot;
import org.testng.annotations.Test;

/**
 * Check histogram values against org.yammer metrics-core.
 */
public class C7731Yammer
{
    @Test
    public void maxValueTest() throws Exception {
        MetricsRegistry metricsRegistry = new MetricsRegistry();
        Histogram histogram01 = metricsRegistry.newHistogram(C7731Yammer.class, "test01", true);
        Histogram histogram1 = metricsRegistry.newHistogram(C7731Yammer.class, "test1", true);
        Histogram histogram10 = metricsRegistry.newHistogram(C7731Yammer.class, "test10", true);

        for (int sec = 0; sec < 12000; sec ++)
        {
            Thread.sleep(100L);
            if (sec % 150 == 0)
            {
                dump(sec, histogram01, "01");
                dump(sec, histogram1, "1");
                dump(sec, histogram10, "10");
            }

            if (sec % 100 == 0)
                histogram01.update(sec < 600 ? 100 : 2);

            if (sec % 10 == 0)
                histogram1.update(sec < 600 ? 100 : 2);

            histogram10.update(sec < 600 ? 100 : 2);
        }
    }

    private void dump(int sec, Histogram histogram, String name)
    {
        Snapshot snapshot = histogram.getSnapshot();
        System.out.printf("%s - second: %3d%n" +
                          "   hist-min/mean/max:  %3.2f %3.2f %3.2f%n"+
                          "   snap-med-999-99-95: %3.2f %3.2f %3.2f %3.2f%n"+
                          "   snap-0-1-9999:      %3.2f %3.2f %3.2f%n",
                          name, sec/10,
                          histogram.min(), histogram.mean(), histogram.max(),
                          snapshot.getMedian(), snapshot.get999thPercentile(), snapshot.get99thPercentile(), snapshot.get95thPercentile(),
                          snapshot.getValue(0d), snapshot.getValue(1d),
                          snapshot.getValue(0.9999d));
    }
}
